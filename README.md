# Portal Aberto Api

Serviço Back-end do Portal Aberto do [Instituto de Psicologia da USP](https://www.usp.br/interpsi/)

Esse projeto utiliza Ruby 2.7.2, Rails 6.1.3 e MongoDB como banco de dados.

Para informações sobre como executar o deploy da aplicação ou executar os testes de unidade, acessar essa [documentação](https://gitlab.com/portal-aberto/portal-aberto-deploy/-/blob/master/README.md).
