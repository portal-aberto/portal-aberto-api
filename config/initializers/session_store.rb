# frozen_string_literal: true

Rails.application.config.session_store :cookie_store, key: '_authentication_app', expire_after: 60.minutes
