# frozen_string_literal: true

Rails.application.routes.draw do
  resources :tags
  get '/books/:book_id/tags', to: 'tags#book_index'

  resources :sessions, only: [:create]
  resources :registrations, only: %i[create confirm_email]

  post '/registrations/confirm_email', to: 'registrations#confirm_email'

  resources :users, only: %i[index show destroy update]
  delete :logout, to: 'sessions#logged_out'
  get :logged_in, to: 'sessions#logged_in'

  post :update_profile, to: 'settings#update_profile'
  delete :delete_own_user, to: 'settings#delete_own_user'

  patch :update_password, to: 'settings#update_password'
  patch :recover_password, to: 'settings#recover_password'
  post :reset_password_link_status, to: 'settings#reset_password_link_status'
  patch :reset_password, to: 'settings#reset_password'

  post '/books/csv', to: 'books#create_csv', via: :post, defaults: { format: :csv }
  patch '/books/csv', to: 'books#update_csv', via: :patch, defaults: { format: :csv }
  resources :books

  patch '/books/:book_id/ratings', to: 'ratings#create_or_update'
  delete '/books/:book_id/ratings', to: 'ratings#destroy'

  post '/uploads/images', to: 'uploads#create_image'
  delete '/uploads/images/:id', to: 'uploads#delete_image'

  post '/books/:book_id/comments', to: 'comments#create'
  get '/books/:book_id/comments', to: 'comments#book_index'
  delete '/books/:book_id/comments/:comment_id', to: 'comments#destroy'

  get '/users/:user_id/comments', to: 'comments#user_index'

  if Rails.env.test?
    namespace :test do
      resource :session, only: [:create]
    end
  end
end
