# frozen_string_literal: true

module RequestSpecHelper
  def json
    JSON.parse(response.body)
  end

  def log_in_as(user_type)
    user = create(:user, user_type: user_type)
    post test_session_path, params: { user_id: user.id }
  end
end
