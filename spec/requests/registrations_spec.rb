# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Registrations', type: :request do
  describe 'user registration' do
    let(:username) { 'Username' }
    let(:email) { 'user@gmail.com' }
    let(:birthDate) { '1996-10-01' }
    let(:gender) { 'M' }
    let(:password) { 'p@ssword' }
    let(:password_confirmation) { 'p@ssword' }

    context 'when the user is valid' do
      before do
        post '/registrations',
             params: { user: { username: username, email: email, birthDate: birthDate, gender: gender,
                               password: password, password_confirmation: password_confirmation } }
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    let(:password_confirmation_wrong) { 'password' }
    context 'when the user password and password_confirmation dont match' do
      before do
        post '/registrations',
             params: { user: { email: email, password: password, password_confirmation: password_confirmation_wrong } }
      end

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end
    end

    context 'trying to register a user that already exists' do
      before do
        post '/registrations',
             params: { user: { email: email, password: password, password_confirmation: password_confirmation } }
      end
      before do
        post '/registrations',
             params: { user: { email: email, password: password, password_confirmation: password_confirmation } }
      end
      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end
    end

    context 'trying to register a user with a blank email' do
      before do
        post '/registrations',
             params: { user: { email: '', password: password, password_confirmation: password_confirmation } }
      end
      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end
    end

    context 'trying to register a user with a blank password' do
      before do
        post '/registrations',
             params: { user: { email: email, password: '', password_confirmation: password_confirmation } }
      end
      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end
    end

    context 'trying to register a user with a blank password confirmation' do
      before do
        post '/registrations', params: { user: { email: email, password: password, password_confirmation: '' } }
      end
      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end
    end
  end
end
