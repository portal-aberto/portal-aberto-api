# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Books API', type: :request do
  let!(:books) { create_list(:book, 10) }
  let(:book_id) { books.first.id }

  let(:book) { books.first }
  let(:book_title) { books.first.title }
  let(:book_authors) { book.authors }
  let(:book_year) { book.year }

  describe 'GET /books' do
    it 'return all books' do
      get '/books'
      expect(response).to have_http_status(200)
      expect(json).to_not be_empty
      expect(json['items'].size).to eq(10)
      expect(json['current_page']).to eq(1)
      expect(json['previous_page']).to eq(nil)
      expect(json['next_page']).to eq(nil)
      expect(json['page_size']).to eq(20)
      expect(json['pages']).to eq(1)
      expect(json['count']).to eq(10)
    end

    it 'return books filtered by title' do
      get '/books', params: { title: book_title }

      expect(response).to have_http_status(200)
      expect(json).to_not be_empty
      expect(json['items'].all? { |book| book['title'] == book_title }).to eq(true)
      expect(json['current_page']).to eq(1)
      expect(json['previous_page']).to eq(nil)
      expect(json['next_page']).to eq(nil)
      expect(json['page_size']).to eq(20)
      expect(json['pages']).to eq(1)
    end

    it 'return books filtered by code' do
      get '/books', params: { code: book.code }

      expect(response).to have_http_status(200)
      expect(json).to_not be_empty
      expect(json['items'].size).to eq(1)
      expect(json['current_page']).to eq(1)
      expect(json['previous_page']).to eq(nil)
      expect(json['next_page']).to eq(nil)
      expect(json['page_size']).to eq(20)
      expect(json['pages']).to eq(1)
      expect(json['count']).to eq(1)
    end

    it 'return books filtered by authors' do
      get '/books', params: { author: book_authors[0] }

      expect(response).to have_http_status(200)
      expect(json).to_not be_empty
      expect(json['items'].all? { |book| book['authors'].include?(book_authors[0]) }).to eq(true)
      expect(json['current_page']).to eq(1)
      expect(json['previous_page']).to eq(nil)
      expect(json['next_page']).to eq(nil)
      expect(json['page_size']).to eq(20)
      expect(json['pages']).to eq(1)
    end

    it 'return books filtered by year' do
      get '/books', params: { year: book_year }

      expect(response).to have_http_status(200)
      expect(json).to_not be_empty
      expect(json['items'].all? { |book| book['year'] == book_year }).to eq(true)
      expect(json['current_page']).to eq(1)
      expect(json['previous_page']).to eq(nil)
      expect(json['next_page']).to eq(nil)
      expect(json['page_size']).to eq(20)
      expect(json['pages']).to eq(1)
    end

    it 'limit result of books based on params' do
      get '/books', params: { per_page: 5 }
      expect(response).to have_http_status(200)
      expect(json).to_not be_empty
      expect(json['items'].size).to eq(5)
      expect(json['current_page']).to eq(1)
      expect(json['previous_page']).to eq(nil)
      expect(json['next_page']).to eq(2)
      expect(json['page_size']).to eq(5)
      expect(json['pages']).to eq(2)
      expect(json['count']).to eq(10)
    end

    it 'get correct page' do
      get '/books', params: { page: 2, per_page: 5 }

      expect(response).to have_http_status(200)
      expect(json).to_not be_empty
      expect(json['items'].size).to eq(5)
      expect(json['current_page']).to eq(2)
      expect(json['previous_page']).to eq(1)
      expect(json['next_page']).to eq(nil)
      expect(json['page_size']).to eq(5)
      expect(json['pages']).to eq(2)
      expect(json['count']).to eq(10)
    end
  end

  describe 'GET /books/:id' do
    before { get "/books/#{book_id}" }

    context 'when the book exists' do
      it 'returns the book' do
        expect(json).not_to be_empty
        expect(json['_id']['$oid']).to eq(book_id.to_s)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the book does not exist' do
      let(:book_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(json['message']).to match('Book not found')
      end
    end
  end

  describe 'POST /books' do
    let(:valid_attributes) { { title: "The Hitchhiker's Guide to the Galaxy", code: '123' } }

    context 'when the request is valid' do
      before { log_in_as(%i[admin moderator].sample) }
      before { post '/books', params: valid_attributes }

      it 'creates a book' do
        expect(json['title']).to eq("The Hitchhiker's Guide to the Galaxy")
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context "when the user isn't manager or admin" do
      before { log_in_as(:user) }
      before { post '/books', params: valid_attributes }

      it 'does not create a book' do
        expect(json['message']).to eq('not allowed to manage? this Class')
      end

      it 'returns status code 401' do
        expect(response).to have_http_status(401)
      end
    end

    context 'when the request is invalid' do
      before { log_in_as(%i[admin moderator].sample) }
      before { post '/books', params: { title: 'Evangelion' } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end
    end
  end

  describe 'PUT /books/:id' do
    let(:valid_attributes) { { title: 'Shopping' } }

    context 'when the record exists' do
      before { log_in_as(%i[admin moderator].sample) }
      before { put "/books/#{book_id}", params: valid_attributes }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end

    context "when the user isn't manager or admin" do
      before { log_in_as(:user) }
      before { put "/books/#{book_id}", params: valid_attributes }

      it 'does not update a book' do
        expect(json['message']).to eq('not allowed to manage? this Class')
      end

      it 'returns status code 401' do
        expect(response).to have_http_status(401)
      end
    end
  end

  describe 'DELETE /books/:id' do
    before { log_in_as(%i[admin moderator].sample) }
    before { delete "/books/#{book_id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end

    context "when the user isn't manager or admin" do
      before { log_in_as(:user) }
      before { delete "/books/#{book_id}" }

      it 'does not delete a book' do
        expect(json['message']).to eq('not allowed to manage? this Class')
      end

      it 'returns status code 401' do
        expect(response).to have_http_status(401)
      end
    end
  end

  # Testes do CSV
  Dir.mkdir 'tmp/csv' unless File.directory?('tmp/csv')
  describe 'POST /books/csv' do
    let(:header1) { 'codigo,titulo,autores,editora,edicao,ano,cidade' }
    let(:row21) { 'CODE1,TITLE1,AUTHORS1,PUBLISHER1,EDITION1,2020,CITY1' }
    let(:row31) { 'CODE2,TITLE2,AUTHORS2,PUBLISHER2,EDITION2,2021,CITY2' }
    let(:rows1) { [header1, row21, row31] }
    let(:file_path1) { 'tmp/csv/test_create_file1.csv' }
    let!(:csv1) do
      CSV.open(file_path1, 'w') do |csv|
        rows1.each do |row|
          csv << row.split(',')
        end
      end
    end

    context 'when the request is valid' do
      before { log_in_as(%i[admin moderator].sample) }
      before { post '/books/csv', params: { csv_file: file_path1 } }

      it 'returns a CSV file of the creation process' do
        expect(response.body).to eq("codigo,status;\nCODE1,Item criado com sucesso!;\nCODE2,Item criado com sucesso!;\n")
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context "when the user isn't manager or admin" do
      before { log_in_as(:user) }
      before { post '/books/csv', params: { csv_file: file_path1 } }

      it 'does not create a book from csv' do
        expect(json['message']).to eq('not allowed to manage? this Class')
      end

      it 'returns status code 401' do
        expect(response).to have_http_status(401)
      end
    end

    let(:header2) { 'codigo,titulo,autores,editora,edicao,cidade,campo_invalido' }
    let(:row22) { 'CODE1,TITLE1,AUTHORS1,PUBLISHER1,EDITION1,CITY1' }
    let(:row32) { 'CODE2,TITLE2,AUTHORS2,PUBLISHER2,EDITION2,CITY2' }
    let(:rows2) { [header2, row22, row32] }
    let(:file_path2) { 'tmp/csv/test_create_file2.csv' }
    let!(:csv2) do
      CSV.open(file_path2, 'w') do |csv|
        rows2.each do |row|
          csv << row.split(',')
        end
      end
    end

    context 'when the request is invalid' do
      before { log_in_as(%i[admin moderator].sample) }
      before { post '/books/csv', params: { csv_file: file_path2 } }

      it 'returns response for a bad header format' do
        expect(response.body).to eq('{"message":"Bad header format"}')
      end

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end
    end
  end

  describe 'PATCH /books/csv' do
    let(:header1) { 'codigo,titulo,autores,editora,edicao,ano,cidade' }
    let(:row21) { 'CODE1,TITLE1,AUTHORS1,PUBLISHER1,EDITION1,2020,CITY1' }
    let(:row31) { 'CODE2,TITLE2,AUTHORS2,PUBLISHER2,EDITION2,2021,CITY2' }
    let(:rows1) { [header1, row21, row31] }
    let(:file_path1) { 'tmp/csv/test_create_file_3.csv' }
    let!(:csv1) do
      CSV.open(file_path1, 'w') do |csv|
        rows1.each do |row|
          csv << row.split(',')
        end
      end
    end

    let(:header2) { 'codigo,titulo,autores,editora,edicao,ano,cidade' }
    let(:row22) { 'CODE1,TITLE1_UPDATED,AUTHORS1,PUBLISHER1,EDITION1,2020,CITY1' }
    let(:row32) { 'CODE2,TITLE2_UPDATED,AUTHORS2,PUBLISHER2,EDITION2,2021,CITY2' }
    let(:rows2) { [header2, row22, row32] }
    let(:file_path2) { 'tmp/csv/test_update_file_1.csv' }
    let!(:csv2) do
      CSV.open(file_path2, 'w') do |csv|
        rows2.each do |row|
          csv << row.split(',')
        end
      end
    end

    before { log_in_as(%i[admin moderator].sample) }
    before { post '/books/csv', params: { csv_file: file_path1 } }

    context 'when the request is valid' do
      before { log_in_as(%i[admin moderator].sample) }
      before { patch '/books/csv', params: { csv_file: file_path2 } }

      it 'returns a CSV file of the update process' do
        expect(response.body).to eq("codigo,status\nCODE1,Item alterado com sucesso!\nCODE2,Item alterado com sucesso!\n")
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    let(:header3) { 'codigo,titulo,autores,editora,edicao,cidade,campo_invalido' }
    let(:row23) { 'CODE1,TITLE1,AUTHORS1,PUBLISHER1,EDITION1,CITY1' }
    let(:row33) { 'CODE2,TITLE2,AUTHORS2,PUBLISHER2,EDITION2,CITY2' }
    let(:rows3) { [header3, row23, row33] }
    let(:file_path3) { 'tmp/csv/test_update_file_2.csv' }
    let!(:csv3) do
      CSV.open(file_path3, 'w') do |csv|
        rows3.each do |row|
          csv << row.split(',')
        end
      end
    end

    context 'when the request is invalid' do
      before { log_in_as(%i[admin moderator].sample) }
      before { patch '/books/csv', params: { csv_file: file_path3 } }

      it 'returns response for a bad header format' do
        expect(response.body).to eq('{"message":"Bad header format"}')
      end

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end
    end
  end
end
