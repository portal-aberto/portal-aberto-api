# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Sessions', type: :request do
  describe 'login' do
    let(:username) { 'Username' }
    let(:email) { 'email@qualquer.com' }
    let(:birthDate) { '1996-10-01' }
    let(:gender) { 'M' }
    let(:password) { 'p@ssword' }
    let(:password_confirmation) { 'p@ssword' }

    context 'when the user exists but e-mail is not confirmed' do
      before do
        post '/registrations',
             params: { user: { username: username, email: email, birthDate: birthDate, gender: gender,
                               password: password, password_confirmation: password_confirmation } }
      end

      before { post '/sessions', params: { user: { email: email, password: password } } }

      it 'returns not logged in status' do
        parsed_body = JSON.parse(response.body)
        expect(parsed_body['logged_in']).to eq(false)
      end

      it 'returns status code 403' do
        expect(response).to have_http_status(401)
      end

      it 'returns e-mail not confirmed' do
        parsed_body = JSON.parse(response.body)
        expect(parsed_body['user']['email_confirmed']).to eq(false)
      end
    end

    context 'when the user dont exists' do
      before { post '/sessions', params: { user: { email: email, password: password } } }
      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end
    end

    let(:password_wrong) { 'password' }
    context 'when the user password is invalid' do
      before { post '/sessions', params: { user: { email: email, password: password_wrong } } }
      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end
    end

    context 'when the user email is blank' do
      before { post '/sessions', params: { user: { email: '', password: password } } }
      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end
    end

    context 'when the user password is blank' do
      before { post '/sessions', params: { user: { email: email, password: '' } } }
      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end
    end

    context 'when the user isnt logged in' do
      before { post '/sessions', params: { user: { email: email, password: password } } }
      before { get '/logged_in' }

      it 'returns logged in status' do
        expect(json['logged_in']).to eq(false)
      end

      it 'returns status code 401' do
        expect(response).to have_http_status(401)
      end
    end
  end

  describe 'logout' do
    context 'successful logout' do
      before { delete '/logout' }
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end
  end
end
