# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Tags API', type: :request do
  let!(:tags) { create_list(:tag, 10) }
  let(:tag_id) { tags.first.id }

  describe 'GET /tags' do
    it 'return all tags' do
      get '/tags', params: { paginate: true }
      expect(response).to have_http_status(200)
      expect(json).to_not be_empty
      expect(json['items'].size).to eq(10)
      expect(json['current_page']).to eq(1)
      expect(json['previous_page']).to eq(nil)
      expect(json['next_page']).to eq(nil)
      expect(json['page_size']).to eq(20)
      expect(json['pages']).to eq(1)
      expect(json['count']).to eq(10)
    end
  end

  describe 'GET /tags/:id' do
    it 'renders a successful response' do
      tag = tags.first
      tag_id = tag.id
      get tag_url(tag)
      expect(response).to be_successful
      expect(json['_id']['$oid']).to eq(tag_id.to_s)
    end
  end

  describe 'POST /tags' do
    let(:valid_attributes) { { value: "The Hitchhiker's Guide to the Galaxy" } }
    context 'when the request is valid' do
      before { log_in_as(%i[admin moderator].sample) }
      before { post '/tags', params: valid_attributes }
      it 'creates a new Tag' do
        expect(json['value']).to eq("The Hitchhiker's Guide to the Galaxy")
      end
      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context "when the user isn't manager or admin" do
      before { log_in_as(:user) }
      before { post '/tags', params: valid_attributes }

      it 'does not create a tag' do
        expect(json['message']).to eq('not allowed to manage? this Class')
      end

      it 'returns status code 401' do
        expect(response).to have_http_status(401)
      end
    end

    context 'when the request is invalid' do
      before { log_in_as(%i[admin moderator].sample) }
      before { post '/tags', params: { value: '1' } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end
    end
  end

  describe 'PUT /tags/:id' do
    let(:valid_attributes) { { value: 'yet another value' } }
    context 'when the record exists' do
      before { log_in_as(%i[admin moderator].sample) }
      before { put "/tags/#{tag_id}", params: valid_attributes }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end

    context "when the user isn't manager or admin" do
      before { log_in_as(:user) }
      before { put "/tags/#{tag_id}", params: valid_attributes }

      it 'does not update a tag' do
        expect(json['message']).to eq('not allowed to manage? this Class')
      end

      it 'returns status code 401' do
        expect(response).to have_http_status(401)
      end
    end
  end

  describe 'DELETE /tags/:id' do
    let(:valid_attributes) { { value: 'yet another value' } }
    before { log_in_as(%i[admin moderator].sample) }
    it 'destroys the requested tag' do
      tag = Tag.create! valid_attributes
      expect do
        delete "/tags/#{tag.id}"
      end.to change(Tag, :count).by(-1)
      expect(response).to have_http_status(204)
    end

    context "when the user isn't manager or admin" do
      before { log_in_as(:user) }
      before { delete "/tags/#{tag_id}" }

      it 'does not delete a book' do
        expect(json['message']).to eq('not allowed to manage? this Class')
      end

      it 'returns status code 401' do
        expect(response).to have_http_status(401)
      end
    end
  end
end
