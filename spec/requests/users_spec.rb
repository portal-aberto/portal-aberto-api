# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Users', type: :request do
  let!(:users) { create_list(:user, 9) }
  let(:user) { users.first }
  let(:user_id) { user.id }
  let(:username) { user.username }
  let(:user_email) { user.email }
  let(:user_type) { user.user_type }

  describe 'GET /users' do
    context 'when the user is logged in' do
      before { log_in_as(:admin) }

      it 'returns all users' do
        get '/users'
        expect(response).to have_http_status(200)
        expect(json).to_not be_empty
        expect(json['items'].size).to eq(10)
        expect(json['current_page']).to eq(1)
        expect(json['previous_page']).to eq(nil)
        expect(json['next_page']).to eq(nil)
        expect(json['page_size']).to eq(20)
        expect(json['pages']).to eq(1)
        expect(json['count']).to eq(10)
      end

      it 'return users filtered by username' do
        get '/users', params: { username: username }

        expect(response).to have_http_status(200)
        expect(json).to_not be_empty
        expect(json['items'].all? { |user| user['username'] == username }).to eq(true)
        expect(json['current_page']).to eq(1)
        expect(json['previous_page']).to eq(nil)
        expect(json['next_page']).to eq(nil)
        expect(json['page_size']).to eq(20)
        expect(json['pages']).to eq(1)
      end

      it 'return users filtered by email' do
        get '/users', params: { email: user_email }

        expect(response).to have_http_status(200)
        expect(json).to_not be_empty
        expect(json['items'].all? { |user| user['email'] == user_email }).to eq(true)
        expect(json['current_page']).to eq(1)
        expect(json['previous_page']).to eq(nil)
        expect(json['next_page']).to eq(nil)
        expect(json['page_size']).to eq(20)
        expect(json['pages']).to eq(1)
      end

      it 'return users filtered by user_type' do
        get '/users', params: { user_type: user_type }

        expect(response).to have_http_status(200)
        expect(json).to_not be_empty
        expect(json['items'].all? { |user| user['user_type'] == user_type }).to eq(true)
        expect(json['current_page']).to eq(1)
        expect(json['previous_page']).to eq(nil)
        expect(json['next_page']).to eq(nil)
        expect(json['page_size']).to eq(20)
        expect(json['pages']).to eq(1)
      end
    end

    context 'when the user is not logged in' do
      before { get '/users' }

      it 'does not list users' do
        expect(json['message']).to eq('not allowed to list? this Class')
      end

      it 'returns status code 401' do
        expect(response).to have_http_status(401)
      end
    end
  end

  describe 'GET /users/:id' do
    context 'when the user exists' do
      before { log_in_as(:admin) }
      before { get "/users/#{user_id}" }
      it 'returns the user' do
        expect(json).not_to be_empty
        expect(json['_id']['$oid']).to eq(user_id.to_s)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end
    context 'when the user does not exist' do
      before { log_in_as(:admin) }
      before { get '/users/100' }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(json['message']).to match('User not found')
      end
    end
    context 'when the user is not logged in' do
      before { get "/users/#{user_id}" }

      it 'does not list users' do
        expect(json['message']).to eq('not allowed to list? this Class')
      end

      it 'returns status code 401' do
        expect(response).to have_http_status(401)
      end
    end
  end

  describe 'PUT /users/:id' do
    context 'when the record exists' do
      before { log_in_as(:admin) }
      before { put "/users/#{user_id}", params: { user_type: :moderator } }

      it 'updates the user_type' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end

    context 'when the user is a manager and user_type is not' do
      before { log_in_as(:moderator) }
      before { put "/users/#{user_id}", params: { user_type: %i[admin user].sample } }

      it 'does not update the user_type' do
        expect(json['message']).to eq('not allowed to manage? this Class')
      end

      it 'returns status code 401' do
        expect(response).to have_http_status(401)
      end
    end

    context "when the user isn't manager or admin" do
      before { log_in_as(:user) }
      before { put "/users/#{user_id}", params: { user_type: :moderator } }

      it 'does not update user_type' do
        expect(json['message']).to eq('not allowed to manage? this Class')
      end

      it 'returns status code 401' do
        expect(response).to have_http_status(401)
      end
    end

    context 'when the user_type is null' do
      before { log_in_as(:admin) }
      before { put "/users/#{user_id}", params: {} }

      it 'does not update the user_type' do
        expect(json['message']).to eq('Cannot update to this user_type')
      end

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end
    end
  end

  describe 'DELETE /users/:id' do
    before { log_in_as(:admin) }
    before { delete "/users/#{user_id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end

    context "when the user isn't  admin" do
      before { log_in_as(%i[user moderator].sample) }
      before { delete "/users/#{user_id}" }

      it 'does not delete a user' do
        expect(json['message']).to eq('not allowed to destroy? this Class')
      end

      it 'returns status code 401' do
        expect(response).to have_http_status(401)
      end
    end
  end
end
