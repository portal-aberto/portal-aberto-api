# frozen_string_literal: true

FactoryBot.define do
  factory :book do
    title { Faker::Book.title }
    code { Faker::Code.npi }
    authors { [Faker::Movies::StarWars.character] }
    publisher { Faker::Book.publisher }
  end
end
