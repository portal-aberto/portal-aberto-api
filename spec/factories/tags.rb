# frozen_string_literal: true

FactoryBot.define do
  factory :tag do
    value { Faker::Code.npi }
  end
end
