# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    username { Faker::Code.npi }
    birthDate { Faker::Book.title }
    email { "#{username}.#{birthDate}@example.com".downcase }
    gender { %i[M F].sample }
    password_digest { Faker::Code.npi }
    user_type { %i[user admin moderator].sample }
  end
end
