# frozen_string_literal: true

module Test
  class SessionsController < ApplicationController
    def create
      id = params[:user_id]
      session[:user_id] = id
    end
  end
end
