# frozen_string_literal: true

class RatingsController < ApplicationController
  # POST '/books/:book_id/ratings'
  def create_or_update
    authorize Rating, :manage?
    @rating = Rating.find_or_create_by(book_id: params[:book_id], user_id: @current_user._id)
    @rating.value = params['value']

    if @rating.save
      Book.find(params[:book_id]).update_rating
      render json: @rating, status: :created
    else
      render json: @rating.errors, status: :unprocessable_entity
    end
  end

  # DELETE '/books/:book_id/ratings'
  def destroy
    authorize Rating, :manage?
    @rating = Rating.find_by(book_id: params[:book_id], user_id: @current_user._id)
    @rating.destroy
    Book.find(params[:book_id]).update_rating
    head :no_content
  end
end
