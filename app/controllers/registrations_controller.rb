# frozen_string_literal: true

class RegistrationsController < ApplicationController
  def create
    user = User.create!(
      username: params['user']['username'].downcase,
      email: params['user']['email'],
      name: params['user']['name'],
      birthDate: params['user']['birthDate'],
      gender: params['user']['gender'],
      password: params['user']['password'],
      password_confirmation: params['user']['password_confirmation']
    )
    if user
      RegistrationMailer.user_registered(user).deliver_now
      head :created
    end
  end

  def confirm_email
    user = User.find_by(confirmation_token: params[:token])
    if user
      user.email_activate
      head :ok
    end
  end
end
