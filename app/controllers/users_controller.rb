# frozen_string_literal: true

class UsersController < ApplicationController
  def index
    authorize User, :list?
    @users = UserFilter.new(User.scoped.descending(:created_at)).apply(params)
    paginate(@users)
  end

  def show
    authorize User, :list?
    id = params[:id]

    @user = User.find(params[:id])
    json_response(@user, :ok)
  end

  def update
    authorize User, :manage?
    id = params[:id]

    user_type = params[:user_type]

    if !User.user_types_s.include?(user_type) || ((user_type != 'moderator') && (@current_user.user_type == :moderator))
      return json_response({ message: 'Cannot update to this user_type' }, :unprocessable_entity)
    end

    @user = User.find(id)
    @user.update!(user_type: user_type)
    head :no_content
  end

  def destroy
    authorize User, :destroy?
    id = params[:id]

    @user = User.find(params[:id])
    @user.comments.destroy
    @user.destroy

    head :no_content
  end
end
