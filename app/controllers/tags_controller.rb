# frozen_string_literal: true

class TagsController < ApplicationController
  before_action :set_tag, only: %i[show update destroy]

  def book_index
    @book = Book.find_by(id: params[:book_id])
    @tags = @book.tag_ids && !@book.tag_ids.empty? ? Tag.scoped.where(:id.in => @book.tag_ids) : []
    json_response(@tags, :ok)
  end

  def index
    @tags = TagFilter.new(Tag.scoped.descending(:created_at)).apply(params)
    if params[:paginate]
      paginate(@tags)
    else
      json_response(@tags, :ok)
    end
  end

  def show
    json_response(@tag, :ok)
  end

  def create
    authorize Tag, :manage?
    @tag = Tag.create!(tag_params)
    json_response(@tag, :created)
  end

  def update
    authorize Tag, :manage?
    @tag.update!(tag_params)
  end

  def destroy
    authorize Tag, :manage?
    @tag.destroy

    head :no_content
  end

  def set_tag
    id = params[:id]

    @tag = Tag.find(params[:id])
  end

  def tag_params
    params.permit(:value)
  end
end
