# frozen_string_literal: true

class SettingsController < ApplicationController
  include CurrentUserConcern

  def update_profile
    if @current_user
      user = @current_user

      if user.profile_picture != nil
        old_profile_pic = user.profile_picture.split("/")[-1]
        new_profile_pic = params[:profile_picture].to_s.split("/")[-1]
      end

      status = user.update(name: params['user']['name'], 
        birthDate: params['user']['birthDate'],
        gender: params['user']['gender'],
        profile_picture: params['user']['profilePic']
      )

      if old_profile_pic != nil && old_profile_pic != '' && status
        if old_profile_pic != new_profile_pic
          image_path = Rails.root.to_s + '/public/uploads/images/' + old_profile_pic
          File.delete(image_path) if File.exist?(image_path)
        end
      end

      return json_response({user: user}, :ok)
    end
  end

  def delete_own_user
    if @current_user
      user = @current_user
      user.comments.destroy
      reset_session
      user.destroy
      json_response({ logged_out: true }, :ok)
    end
  end

  def update_password
    if @current_user
      user = @current_user
      verifyCurrentPassword = user.try(:authenticate, params['user']['current_password'])
      if verifyCurrentPassword
        new_password = params['user']['new_password']
        password_confirmation = params['user']['new_password_confirmation']
        if new_password == password_confirmation
          user.password = new_password
          user.password_confirmation = password_confirmation
          user.save
          head :ok
        else
          head :unprocessable_entity
        end
      else
        head :unauthorized
      end
    end
  end

  def recover_password
    user = User.find_by(email: params['user']['email'])
    if user
      user.create_password_reset_token
      PasswordRecoveryMailer.password_recovery(user).deliver_now
      head :accepted
    end
  end

  def reset_password_link_status
    user = User.find_by(password_reset_token: params['token'])
    if user
      if user.password_reset_token_expiration > Time.now
        head :ok
      else
        head :unauthorized
      end
    end
  end

  def reset_password
    user = User.find_by(password_reset_token: params['user']['token'])
    if user
      new_password = params['user']['new_password']
      password_confirmation = params['user']['new_password_confirmation']
      if new_password == password_confirmation
        user.password = new_password
        user.password_confirmation = password_confirmation
        user.password_reset_token = nil
        user.password_reset_token_expiration = nil
        user.save
        head :ok
      else
        head :unprocessable_entity
      end
    else
      head :unauthorized
    end
  end
end
