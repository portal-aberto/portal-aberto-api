# frozen_string_literal: true

class CommentsController < ApplicationController
  def book_index
    @book = Book.find_by(id: params[:book_id])
    paginate(@book.comments.descending(:created_at))
  end

  def user_index
    @user = User.find_by(id: params[:user_id])
    paginate(@user.comments.descending(:created_at))
  end

  def create
    @book = Book.find(params[:book_id])
    @comment = @book.comments.create(book_id: params[:book_id], user_id: @current_user._id,
                                     username: @current_user.username, body: params[:body],
                                     book_title: @book.title)
    authorize @comment, :manage?
    @comment.user_id = @current_user.id
    if @comment.save
      head :created

    else
      head :unprocessable_entity
    end
  end

  def destroy
    @comment = Comment.find_by(id: params[:comment_id])
    authorize @comment, :manage?
    if @comment.user_id = @current_user._id
      @comment.destroy
      head :ok
    else
      head :unprocessable_entity
    end
  end
end
