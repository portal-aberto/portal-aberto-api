# frozen_string_literal: true

# app/controllers/concerns/exception_handler.rb
module ExceptionHandler
  extend ActiveSupport::Concern

  included do
    rescue_from Mongoid::Errors::DocumentNotFound do |e|
      json_response({ message: "#{e.klass} not found" }, :not_found)
    end

    rescue_from Mongoid::Errors::Validations do |e|
      json_response({ message: e.document.errors.full_messages }, :unprocessable_entity)
    end

    rescue_from Pundit::NotAuthorizedError do |e|
      json_response({ message: e.message }, :unauthorized)
    end

  rescue StandardError do |e|
           json_response({ message: e }, :internal_server_error)
         end
  end
end
