# frozen_string_literal: true

module CurrentUserConcern
  extend ActiveSupport::Concern
  included do
    before_action :set_current_user
  end

  def current_user
    User.find(id: session[:user_id]) if session[:user_id]
  rescue Mongoid::Errors::DocumentNotFound
    nil
  end

  def set_current_user
    @current_user = current_user
  end
end
