# frozen_string_literal: true

module Response
  extend ActiveSupport::Concern

  def json_response(object, status = :ok)
    render json: object, status: status
  end

  def csv_response(object, filename, status = :ok)
    render csv: object, filename: filename, status: status
  end

  def paginate(scope, default_per_page = 20)
    collection = scope.page(params[:page]).per((params[:per_page] || default_per_page).to_i)

    current = collection.current_page
    total = collection.total_pages
    page_size = collection.limit_value

    result = {
      items: collection,
      current_page: current,
      previous_page: (current > 1 ? (current - 1) : nil),
      next_page: (current == total ? nil : (current + 1)),
      page_size: page_size,
      pages: total,
      count: collection.total_count
    }
    json_response(result, :ok)
  end
end
