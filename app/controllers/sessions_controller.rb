# frozen_string_literal: true

class SessionsController < ApplicationController
  def create
    user = User.find_by(email: params['user']['email']).try(:authenticate, params['user']['password'])

    if user && user.email_confirmed == true
      session[:user_id] = user.id
      json_response({ logged_in: true, user: user }, :created)
    elsif user && user.email_confirmed == false
      json_response({ logged_in: false, cred: true, user: user }, :unauthorized)
    else 
      json_response({ logged_in: false, cred: false }, :unauthorized)
    end
  end

  def logged_in
    if @current_user
      json_response({ logged_in: true, user: @current_user }, :ok)
    else
      json_response({ logged_in: false }, :unauthorized)
    end
  end

  def logged_out
    reset_session
    json_response({ logged_out: true }, :ok)
  end
end
