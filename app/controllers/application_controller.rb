# frozen_string_literal: true

class ApplicationController < ActionController::API
  include CurrentUserConcern
  skip_before_action :verify_authenticity_token, raise: false

  include ActionController::MimeResponds
  include Response
  include ExceptionHandler
  include Pundit
end
