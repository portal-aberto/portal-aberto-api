# frozen_string_literal: true

class BooksController < ApplicationController
  def index
    sort = params[:sort_by] || :created_at
    order = params[:order] || 'descending'

    @books = BookFilter.new(Book.scoped.send(order, sort)).apply(params)
    paginate(@books)
  end

  def show
    id = params[:id]

    @book = Book.find(params[:id])
    json_response(@book, :ok)
  end

  def create
    authorize Book, :manage?
    @book = Book.create!(book_params)
    json_response(@book, :created)
  end

  def update
    authorize Book, :manage?
    id = params[:id]

    @book = Book.find(params[:id])

    if @book.cover != nil
      old_cover = @book.cover.split("/")[-1]
      new_cover = params[:cover].to_s.split("/")[-1]
    end

    status = @book.update(book_params)

    if old_cover != nil && old_cover != '' && status
      if old_cover != new_cover
        image_path = Rails.root.to_s + '/public/uploads/images/' + old_cover
        File.delete(image_path) if File.exist?(image_path)
      end
    end

    head :no_content
  end

  def destroy
    authorize Book, :manage?
    id = params[:id]

    @book = Book.find(params[:id])
    @book.comments.destroy
    @book.destroy

    head :no_content
  end

  def create_csv
    authorize Book, :manage?
    csv_file = params[:csv_file]
    create_result_csv = Book.create_from_csv(csv_file)
    return json_response({ message: 'Bad header format' }, :unprocessable_entity) unless create_result_csv

    csv_response(create_result_csv, 'create_result.csv')
  end

  def update_csv
    authorize Book, :manage?
    csv_file = params[:csv_file]
    update_result_csv = Book.update_from_csv(csv_file)
    return json_response({ message: 'Bad header format' }, :unprocessable_entity) unless update_result_csv

    csv_response(update_result_csv, 'update_result.csv')
  end

  def book_params
    params.permit(:title, :code, { authors: [] }, { tag_ids: [] }, :edition, :publisher, :year, :city, :cover, :isbn,
                  :description)
  end
end
