# frozen_string_literal: true

class UploadsController < ApplicationController
  def create_image
    authorize :uploads, :manage?
    uploader = ImageUploader.new
    File.open(params[:file]) do |file|
      uploader.store!(file)
      a = uploader.methods
      json_response(uploader.url)
    end
  end

  def delete_image
    authorize :uploads, :manage?
    uploader = ImageUploader.new
    file_path = "public/#{uploader.store_dir}/#{params[:id]}"
    File.delete(file_path) if File.exist?(file_path)
    json_response(nil, status = :no_content)
  end
end
