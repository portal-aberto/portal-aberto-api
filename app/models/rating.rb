# frozen_string_literal: true

class Rating
  include Mongoid::Document
  field :value, type: Integer
  belongs_to :book
  belongs_to :user

  validates_numericality_of :value, greater_than_or_equal_to: 0, less_than_or_equal_to: 5
  validates_uniqueness_of :book, scope: :user
end
