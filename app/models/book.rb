# frozen_string_literal: true

class Book
  include Mongoid::Document
  include Mongoid::Timestamps

  field :code, type: String
  field :isbn, type: String
  field :title, type: String
  field :authors, type: Array
  field :publisher, type: String
  field :edition, type: String
  field :year, type: String
  field :city, type: String
  field :cover, type: String
  field :tag_ids, type: Array
  field :description, type: String
  field :rating, type: Float
  field :votes, type: Integer

  has_many :comments

  validates :code, presence: true, uniqueness: true, length: { minimum: 2, maximum: 40 }
  before_save :validate_tags

  def validate_tags
    if tag_ids && !tag_ids.empty?
      self.tag_ids = Tag.where(:id.in => tag_ids).only(:id).entries.map { |document| document._id.to_s }
    end
  end

  def update_rating
    ratings = Rating.where(book_id: id)
    self.rating = ratings.map(&:value).reduce(:+).to_f / ratings.size
    self.votes = ratings.size
    save
  end

  def self.create_from_csv(file)
    return false unless has_valid_csv_header(file)

    headers = %w[codigo status;]
    CSV.generate(headers: true) do |writer|
      writer << headers
      CSV.foreach(file, headers: true) do |row|
        Book.create!(
          code: row['codigo'],
          isbn: row['isbn'],
          title: row['titulo'],
          authors: row['autores'] != nil ? row['autores'].split(';') : [],
          publisher: row['editora'],
          edition: row['edicao'],
          city: row['cidade'],
          year: row['ano'],
          description: row['descricao']
        )
        writer << [row['codigo'], 'Item criado com sucesso!;']
      rescue StandardError => e
        writer << [row['codigo'], "#{e.message};"]
      end
    end
  end

  def self.update_from_csv(file)
    return false unless has_valid_csv_header(file)

    headers = %w[codigo status]
    CSV.generate(headers: true) do |writer|
      writer << headers
      CSV.foreach(file, headers: true) do |row|
        book = Book.find_by(code: row['codigo'])
        book.update(
          code: row['codigo'],
          isbn: row['isbn'],
          title: row['titulo'],
          authors: row['autores'] != nil ? row['autores'].split(';') : [],
          publisher: row['editora'],
          edition: row['edicao'],
          city: row['cidade'],
          year: row['ano'],
          description: row['descricao']
        )
        writer << [row['codigo'], 'Item alterado com sucesso!']
      rescue StandardError => e
        writer << [row['codigo'], e.message]
      end
    end
  end

  def self.has_valid_csv_header(file)
    headers = CSV.open(file, 'r', &:first)
    headers.size <= 9 and (headers - %w[codigo isbn titulo autores editora edicao cidade ano descricao]).empty?
  end
end
