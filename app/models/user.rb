# frozen_string_literal: true

class User
  extend Enumerize
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::SecurePassword

  before_create :create_confirmation_token

  field :username, type: String
  field :profile_picture, type: String
  field :email, type: String
  field :password_digest, type: String
  field :user_type
  field :email_confirmed, type: Boolean, default: false
  field :confirmation_token, type: String
  field :password_reset_token, type: String
  field :password_reset_token_expiration, type: Time
  enumerize :user_type, in: %i[user admin moderator], default: :user, predicates: true

  field :name, type: String
  field :birthDate, type: Date
  field :gender, type: String

  has_many :comments

  has_secure_password

  validates_presence_of :username
  validates_presence_of :email
  validates_presence_of :user_type

  validates_uniqueness_of :username
  validates_uniqueness_of :email

  validates :username, format: { with: /\A[a-z0-9]+\z/ }
  validates :password, length: { minimum: 8 }
 
  def self.user_types_s
    %w[user admin moderator]
  end

  def create_confirmation_token
    self.confirmation_token = SecureRandom.urlsafe_base64.to_s if confirmation_token.blank?
  end

  def email_activate
    self.email_confirmed = true
    self.confirmation_token = nil
    save!
  end

  def create_password_reset_token
    self.password_reset_token = SecureRandom.urlsafe_base64.to_s
    self.password_reset_token_expiration = Time.now + 1.day
    save!
  end
end
