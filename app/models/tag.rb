# frozen_string_literal: true

class Tag
  include Mongoid::Document
  include Mongoid::Timestamps
  field :value, type: String

  validates :value, presence: true, uniqueness: true, length: { minimum: 2, maximum: 100 }

  before_destroy :remove_from_books

  def remove_from_books
    tag_id = _id.to_s
    Book.where(tag_ids: tag_id).pull(tag_ids: tag_id)
  end
end
