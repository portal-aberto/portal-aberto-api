# frozen_string_literal: true

class Comment
  include Mongoid::Document
  include Mongoid::Timestamps

  field :username, type: String
  field :body, type: String
  field :created_at, type: String
  field :updated_at, type: String
  field :book_title, type: String
  belongs_to :book
  belongs_to :user
end
