# frozen_string_literal: true

class BookPolicy < ApplicationPolicy
  attr_reader :user, :book

  def initialize(user, book)
    @user = user
    @book = book
  end

  def manage?
    @user and (@user.moderator? or @user.admin?)
  end
end
