# frozen_string_literal: true

class UserPolicy < ApplicationPolicy
  attr_reader :current_user, :user

  def initialize(current_user, user)
    @current_user = current_user
    @user = user
  end

  def list?
    @current_user and (@current_user.admin?)
  end

  def manage?
    @current_user and (@current_user.admin?)
  end

  def destroy?
    @current_user and (@current_user.admin?)
  end
end
