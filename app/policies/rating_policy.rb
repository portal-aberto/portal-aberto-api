# frozen_string_literal: true

class RatingPolicy < ApplicationPolicy
  attr_reader :user, :rating

  def initialize(user, rating)
    @user = user
    @rating = rating
  end

  def manage?
    @user
  end
end
