# frozen_string_literal: true

UploadsPolicy = Struct.new(:user, :uploads) do
  attr_reader :user, :uploads

  def initialize(user, uploads)
    @user = user
    @uploads = uploads
  end

  def manage?
    @user and (@user.moderator? or @user.admin?)
  end
end
