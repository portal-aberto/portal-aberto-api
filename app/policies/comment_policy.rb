# frozen_string_literal: true

class CommentPolicy < ApplicationPolicy
  attr_reader :user, :comment

  def initialize(user, comment)
    @user = user
    @comment = comment
  end

  def manage?
    @user and ((@user.moderator? or @user.admin?) or @comment.user_id == @user.id)
  end
end
