# frozen_string_literal: true

class TagFilter
  def initialize(initial_scope)
    @initial_scope = initial_scope
  end

  def apply(params)
    scoped = filter_by_value(@initial_scope, params[:value])
    filter_by_ids(scoped, params[:ids])
  end

  def filter_by_value(scoped, query = nil)
    query ? scoped.where(value: /#{query}/i) : scoped
  end

  def filter_by_ids(scoped, query = nil)
    query ? scoped.where(:id.in => query) : scoped
  end
end
