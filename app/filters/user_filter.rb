# frozen_string_literal: true

class UserFilter
  def initialize(initial_scope)
    @initial_scope = initial_scope
  end

  def apply(params)
    scope = filter_by_username(@initial_scope, params[:username])
    scope = filter_by_email(scope, params[:email])
    filter_by_user_type(scope, params[:user_type])
  end

  private

  def filter_by_username(scoped, query = nil)
    query ? scoped.where(username: /#{query}/i) : scoped
  end

  def filter_by_email(scoped, query = nil)
    query ? scoped.where(email: /#{query}/i) : scoped
  end

  def filter_by_user_type(scoped, query = nil)
    query ? scoped.where(user_type: query.to_s) : scoped
  end
end
