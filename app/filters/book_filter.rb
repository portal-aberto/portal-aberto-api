# frozen_string_literal: true

class BookFilter
  def initialize(initial_scope)
    @initial_scope = initial_scope
  end

  def apply(params)
    scoped = filter_by_title(@initial_scope, params[:title])
    scoped = filter_by_code(scoped, params[:code])
    scoped = filter_by_authors(scoped, params[:author])
    scoped = filter_by_year(scoped, params[:year])
    scoped = filter_by_tags(scoped, params[:tag_ids])
    filter_by_q(scoped, params[:query])
  end

  private

  def filter_by_q(scoped, query = nil)
    query ? scoped.or({ title: /#{query}/i }, { code: /#{query}/i }, {authors: /#{query}/i}) : scoped
  end

  def filter_by_title(scoped, query = nil)
    query ? scoped.where(title: /#{query}/i) : scoped
  end

  def filter_by_code(scoped, query = nil)
    query ? scoped.where(code: /#{query}/i) : scoped
  end

  def filter_by_authors(scoped, query = nil)
    query ? scoped.where(authors: /#{query}/i) : scoped
  end

  def filter_by_year(scoped, query = nil)
    query ? scoped.where(year: query) : scoped
  end

  def filter_by_tags(scoped, query = nil)
    query ? scoped.where(:tag_ids.in => query.split(',')) : scoped
  end
end
