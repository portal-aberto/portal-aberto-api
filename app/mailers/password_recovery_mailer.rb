# frozen_string_literal: true

class PasswordRecoveryMailer < ApplicationMailer
  default from: 'portalabertousp@gmail.com'
  def password_recovery(user)
    @user = user
    mail(
      to: @user.email,
      subject: 'Redefina sua senha'
    )
  end
end
