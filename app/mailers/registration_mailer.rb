# frozen_string_literal: true

class RegistrationMailer < ApplicationMailer
  default from: 'portalabertousp@gmail.com'
  def user_registered(user)
    @user = user
    mail(
      to: @user.email,
      subject: 'Confirme seu e-mail'
    )
  end
end
